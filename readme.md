# Customer Location

This application allows you to register customers and search for their locations.

## Setup

Create an `.env` file based on the [.env_example](/.env_example). Customize the variables as you see fit.

Use `docker-compose` to build the app.

```shell
docker-compose up --build
```

Once the build is done, head over (localhost:8000/docs)[http://localhost:8000/docs].

#### Notes

* Leave `DEBUG` variable set to `True` as Django is not configured to serve static files.
* Errors might appear on the first build because Postgres takes a while to setup. If it happens, wait util Postgres inits, stop the build (`ctrl + c`) and then build again.

### Importing customers from a `csv` file

All you have to do is, have a `csv` file with your customers and use `import customers` command. Because we are using `docker-compose`, you are going to have to put your `csv` file inside project folder and pass on the path from there.

Use the following command:

```bash
docker-compose exec web python3 manage.py import_customers path/to/csv/file
```

Make sure that your file the following information:

| first_name | last_name | email  | gender  |  company | city | title |
|:----------:|-----------|--------|---------|----------|------|-------|

