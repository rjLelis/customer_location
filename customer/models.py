from django.contrib.gis.db import models
from django.db.models.signals import post_save
from django.contrib.gis.geos import Point


class Gender(models.TextChoices):
    FEMALE = 'F', 'Female'
    MALE = 'M', 'Male'
    NONE = 'N', 'None'

class Customer(models.Model):

    def __init__(self, *args, **kwargs):
        super(Customer, self).__init__(*args, **kwargs)

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(unique=True)
    gender = models.CharField(max_length=1, choices=Gender.choices,
        default=Gender.NONE)
    company = models.CharField(max_length=50)
    city = models.CharField(max_length=30)
    title = models.CharField(max_length=50, null=True)
    location = models.PointField(geography=True, default=Point(0.0, 0.0))

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    @property
    def latitude(self):
        return self.location.y

    @property
    def longitude(self):
        return self.location.x

    def __set_gender(self):
        if self.gender not in [gender[0] for gender in Customer.GENDERS]:
            new_gender = {
                'female': Customer.FEMALE,
                'male': Customer.MALE
            }.get(self.gender.lower(), Customer.NONE)
            self.gender = new_gender

    def save(self, *args, **kwargs):
        return super(Customer, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.full_name}'

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=models.Q(gender__in=Gender.values),
                name='%(app_label)s_%(class)s_gender_valid'
            )
        ]
