from django.core.management.base import BaseCommand
from django.utils import timezone

import customer.helpers as customer_helpers
from customer.tasks import find_customer_geolocation
from customer.models import Customer


class Command(BaseCommand):
    help = 'Imports customers to the database from a csv file'

    def add_arguments(self, parser):
        parser.add_argument('file', type=str,
            help='csv file of customers to import to the database')

    def handle(self, *args, **kwargs):
        input_file = kwargs['file']

        try:
            customers = customer_helpers.read_customer_csv(input_file)
            self.stdout.write(f"Importing customers from '{input_file}'...")

            customer_helpers.create_customers_batch(customers)

            self.stdout.write(self.style.SUCCESS(
                    f'{len(customers)} customers imported successfuly'))

        except FileNotFoundError:
            self.stderr.write(f"File '{input_file}' not found")
        except Exception as e:
            self.stderr(e)
