from django.test import TestCase
from django.db import utils as db_utils

from customer.models import Customer
from customer.tasks import find_customer_geolocation


class TasksTestCase(TestCase):

    def setUp(self):
        self.customer = Customer.objects.create(
            first_name='Renato',
            last_name='Lelis',
            email='testemail@test.com',
            gender='M',
            company='The test company',
            city='Recife, Pe',
            title='Backend Developer'
        )

    def test_find_customer_geolocation(self):
        customer = self.customer

        find_customer_geolocation(customer.id)

        customer.refresh_from_db()

        self.assertEquals(customer.longitude, -34.8781517)
        self.assertEquals(customer.latitude, -8.0641931)

    def test_find_customer_geolocation_not_found(self):
        customer = self.customer

        customer.city = 'test_city'
        customer.save()
        find_customer_geolocation(customer.id)

        customer.refresh_from_db()
        self.assertEquals(customer.longitude, 0.0)
        self.assertEquals(customer.latitude, 0.0)
