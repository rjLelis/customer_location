from django.contrib.gis.geos import Point
from django.test import TestCase

from customer.models import Customer


class ModelsTestCase(TestCase):

    def setUp(self):
        self.customer = Customer.objects.create(
            first_name='Renato',
            last_name='Lelis',
            email='testemail@test.com',
            gender='M',
            company='The test company',
            city='Recife, Pe',
            title='Backend Developer'
        )

    def test_customer_full_name(self):

        customer = self.customer

        self.assertEquals(customer.full_name, 'Renato Lelis')

    def test_customer_location(self):
        customer = self.customer

        recife_location = Point(-34.8781517, -8.0641931)
        customer.location = recife_location
        customer.save()

        self.assertEquals(customer.longitude, -34.8781517)
        self.assertEquals(customer.latitude, -8.0641931)

    def test_customer_gender_field(self):
        customer = self.customer

        customer.gender = 'F'
        customer.save()
        self.assertEquals(customer.gender, 'F')
