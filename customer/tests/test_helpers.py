import os

from django.conf import settings
from django.test import TestCase

from customer.models import Customer
import customer.helpers as customer_helpers


class HelpersTestCase(TestCase):

    def setUp(self):
        self.FILES_DIR = os.path.abspath(
            os.path.join(settings.BASE_DIR, 'customer', 'tests', 'test_files'))
        self.csv_file = os.path.join(self.FILES_DIR, 'customers.csv')

        self.customer = {
            'first_name': 'Test',
            'last_name': 'The test',
            'email': 'testemail@test.com',
            'gender': 'F',
            'company': 'the test company',
            'city': 'testvile',
        }
        Customer.objects.create(**self.customer)

    def test_customers_file(self):

        customers = customer_helpers.read_customer_csv(self.csv_file)

        self.assertIsNotNone(customers)
        self.assertAlmostEquals(len(customers), 5)

    def test_customer_file_without_id(self):
        customers = customer_helpers.read_customer_csv(self.csv_file)

        customer = customers[0]

        self.assertNotIn('id', customer)

    def test_customer_file_not_supported(self):
        not_supported_file = os.path.join(self.FILES_DIR, 'customers.txt')

        with self.assertRaises(AssertionError):
            customer_helpers.read_customer_csv(not_supported_file)

    def test_create_customer(self):
        customer_dict = {
            'first_name': 'Test',
            'last_name': 'The test',
            'email': 'test@test.com',
            'gender': 'F',
            'company': 'the test company',
            'city': 'testvile',
        }

        new_customer, created = customer_helpers.create_or_update_customer(
                **customer_dict)

        self.assertTrue(created)
        self.assertIsNotNone(new_customer)
        self.assertEquals(new_customer.full_name, 'Test The test')

    def test_update_customer(self):
        self.customer['email'] = 'testemail@test.com'

        self.customer['company'] = 'Other test company'


        customer_updated, created = customer_helpers.create_or_update_customer(
                **self.customer)

        self.assertFalse(created)
        self.assertEquals(customer_updated.company, 'Other test company')

    def test_create_customers_batch(self):
        random_customers = []

        for i in range(5):
            random_customer = {
                'first_name': f'Test{i}',
                'last_name': f'The test{i}',
                'email': f'test{i}@test.com',
                'gender': 'F',
                'company': 'the test company',
                'city': 'testvile',
            }
            random_customers.append(random_customer)

        random_customers_created = customer_helpers.create_customers_batch(
            random_customers
        )

        self.assertEquals(len(random_customers_created), 5)
