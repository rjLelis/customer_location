from rest_framework.test import APITestCase
from django.urls import reverse
from customer.models import Customer

class ViewsTestCase(APITestCase):

    def setUp(self):
        self.customer = Customer.objects.create(
            first_name='New',
            last_name='Customer',
            email='newcustomer@email.com',
            gender='N',
            company='Customers, inc',
            city='Nowhere',
        )

    def test_list_customers(self):
        list_customers_url = reverse('customer-list')

        response = self.client.get(list_customers_url)

        self.assertEquals(response.status_code, 200)

    def test_retrieve_customer(self):
        retrieve_customer_url = reverse('customer-detail', kwargs={'pk':
            self.customer.id})

        response = self.client.get(retrieve_customer_url)

        self.assertEquals(response.status_code, 200)
