import os

from django.core.management import call_command
from django.conf import settings
from django.test import TestCase

from customer.management import commands
from customer.models import Customer


class CommandsTestCase(TestCase):

    def test_import_customers_command(self):

        TEST_FOLDER = os.path.abspath(os.path.join(settings.BASE_DIR,
            'customer', 'tests', 'test_files'))

        call_command('import_customers',
            os.path.join(TEST_FOLDER, 'customers.csv'))

        customers = Customer.objects.all()

        self.assertEquals(len(customers), 5)
