from django.contrib.gis.geos import Point
from rest_framework.test import APITestCase

from customer.models import Customer
from customer.serializers import CustomerSerializer


class SerializersTestCase(APITestCase):

    def setUp(self):

        self.customer_attributes = {
            'first_name': 'Test',
            'last_name': 'User',
            'email': 'testuser@test.com',
            'gender': 'F',
            'company': 'The test company',
            'city': 'Testville',
            'title': 'Tester',
            'location': Point(8.423, -3124.0)
        }

        self.serializer_data = {
            'full_name': 'Another User',
            'email': 'anotheruser@test.com',
            'gender_name': 'Male',
            'company': 'The test company',
            'city': 'Testville',
            'title': 'Tester',
            'longitude': 23123,
            'latitude': 32423432
        }

        self.customer = Customer.objects.create(**self.customer_attributes)

        self.serializer = CustomerSerializer(instance=self.customer)

    def test_contains_expected_fields(self):
        data = self.serializer.data

        self.assertCountEqual(data.keys(),
        ['full_name', 'email', 'gender_name', 'company', 'city', 'title', 'latitude', 'longitude'])
        self.assertNotIn('gender', data.keys())

    def test_gender_representation(self):
        data = self.serializer.data

        self.assertEquals(data['gender_name'], 'Female')
