from rest_framework import serializers
from .models import Customer

class CustomerSerializer(serializers.ModelSerializer):

    gender_name = serializers.CharField(
        source='get_gender_display', read_only=True)

    class Meta:
        model = Customer
        fields = ('full_name', 'email', 'gender',
        'gender_name', 'company', 'title', 'city', 'longitude', 'latitude')
        extra_kwargs = {
            'gender': {
                'write_only': True
            }
        }
