import csv

from .models import Customer, Gender
from django.db import utils as db_utils
from .tasks import find_customer_geolocation


def create_or_update_customer(**new_customer):
    try:
        email = new_customer.pop('email')
        customer, created = Customer.objects.get_or_create(
            email=email, defaults=new_customer
        )
        if created:
            find_customer_geolocation.delay(customer.id)
        else:
            customer.first_name = new_customer.get('first_name',
                customer.first_name)
            customer.last_name = new_customer.get('last_name',
                customer.last_name)
            customer.email = new_customer.get('email', customer.email)
            customer.gender = new_customer.get('gender', customer.gender)
            customer.company = new_customer.get('company', customer.company)
            customer.title = new_customer.get('title', customer.title)

            city = new_customer.get('city')

            if city and customer.city != city:
                customer.city = city
                find_customer_geolocation.delay(customer.id)

            customer.save()

        return customer, created

    except db_utils.IntegrityError as e:
        raise Exception(e)


def create_customers_batch(customers_to_create):
    try:
        customers = (Customer(**customer) for customer in customers_to_create)

        customers_created = Customer.objects.bulk_create(customers)
        for customer in customers_created:
            find_customer_geolocation.delay(customer.id)

        return customers_created
    except db_utils.IntegrityError as e:
        raise Exception(e)


def read_customer_csv(filepath, csv_delimiter=','):

    assert filepath.endswith('.csv'), 'not a csv file'

    with open(filepath, 'r') as file:
        reader = csv.DictReader(file, delimiter=csv_delimiter)
        customers = [c for c in reader]
        for customer in customers:
            # To avoid conflic with autoincrement id field
            _id = customer.pop('id')

            gender = customer.get('gender', Gender.NONE)
            customer['gender'] = \
                Gender[gender.upper()].value if gender in Gender.labels else Gender.NONE
    return customers

