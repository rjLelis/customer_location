from rest_framework import viewsets
from .models import Customer
from .serializers import CustomerSerializer

class CustomerListRetrieveView(viewsets.ReadOnlyModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
