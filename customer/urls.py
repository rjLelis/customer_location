from rest_framework.routers import SimpleRouter
from .views import CustomerListRetrieveView

router = SimpleRouter()

router.register('customers', CustomerListRetrieveView)

urlpatterns = router.urls
