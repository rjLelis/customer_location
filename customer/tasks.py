from celery import task
from celery.utils.log import get_task_logger
from django.contrib.gis.geos import Point
from geopy import Nominatim
from geopy.exc import (GeocoderNotFound, GeocoderQuotaExceeded,
                       GeocoderTimedOut, GeocoderUnavailable)

from .models import Customer

logger = get_task_logger(__name__)

@task(autoretry_for=(
GeocoderQuotaExceeded, GeocoderTimedOut,
GeocoderUnavailable),
retry_kwargs={'max_retries': 5}, retry_backoff=True)
def find_customer_geolocation(customer_id):
    try:
        customer = Customer.objects.get(id=customer_id)
        geolocator = Nominatim(user_agent='customer_location')

        location = geolocator.geocode(customer.city)

        if not location:
            logger.error(
                f'Location \'{customer.city}\' from customer {customer.full_name} could not be found ')
            return
        customer.location = Point(location.longitude, location.latitude)
        customer.save()

        logger.info(f'Location of customer with id {customer_id} updated')
    except Customer.DoesNotExist:
        logger.error(f'Customer with id {customer_id} does not exists')
