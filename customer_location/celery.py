import os

from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'customer_location.settings')
app = Celery('customer_location')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()
