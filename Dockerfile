FROM makinacorpus/geodjango:focal-3.8

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code

RUN python3 -m pip install --upgrade pip

ADD requirements.txt /code
RUN pip install -r requirements.txt

ADD . /code
